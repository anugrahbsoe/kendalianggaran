<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Realisasi extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_realisasi');
			$this->load->model('m_anggaran');
			$this->load->model('m_pptk');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$id_ta=id_ta();
		$id_pegawai=$this->input->get('id_pegawai');
		if($id_pegawai!=''){
			$this->session->set_userdata('id_pegawai',$id_pegawai);
		}
		elseif($this->session->userdata('id_pegawai')!=''){
			$id_pegawai=$this->session->userdata('id_pegawai');
		}
		$this->db->where('b.id_pegawai',$id_pegawai);
		$get_data=$this->m_realisasi->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','nama belanja','realisasi','tanggal realisasi','tanggal simpan','status','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->nama_belanja,
													uangindonesia($row->realisasi,'Rp.'),
													standar_tanggal($row->tanggal_realisasi),
													standar_tanggal($row->tanggal_simpan),
													$row->status,
													array('data'=>'<div class="btn-group">
                            <a href="'.site_url('realisasi?ubah&id='.$row->id_realisasi).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data Realisasi"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('realisasi/hapus?id='.$row->id_realisasi).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'150px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$databody['id_ta']=$id_ta;
		$databody['id_pegawai']=$id_pegawai;
		$data['title']='Data Realisasi';
		$data['body']=$this->load->view('v_realisasi',$databody,true);
		$data['js']=$this->load->view('js/js_realisasi',$databody,true);
		$this->load->view('html/html',$data);
	}

	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$data['tanggal_simpan']=date('Y-m-d');
			$this->m_realisasi->insert($data);
		}
		redirect('realisasi');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_realisasi=$this->input->post('id_realisasi');
			$where=array('id_realisasi'=>$id_realisasi);
			$this->m_realisasi->update($data,$where);
		}
		redirect('realisasi');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_realisasi' => $this->input->get('id'),
		);
		$this->m_realisasi->delete($where);
		redirect('realisasi');
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_realisasi = $this->input->post('id_realisasi');
		$id_anggaran = $this->input->post('id_anggaran');
		$id_pengguna = id_pengguna();
		$id_ta = $this->input->post('id_ta');
		$realisasi = $this->input->post('realisasi');
		$tanggal_realisasi = $this->input->post('tanggal_realisasi');
		$status = $this->input->post('status');
		$data=array(
			'id_realisasi' => $id_realisasi,
			'id_anggaran' => $id_anggaran,
			'id_pengguna' => $id_pengguna,
			'id_ta' => $id_ta,
			'realisasi' => $realisasi,
			'tanggal_realisasi' => $tanggal_realisasi,
			'status' => $status,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 12-06-2017:23:15:39  **/
/**************************************/
