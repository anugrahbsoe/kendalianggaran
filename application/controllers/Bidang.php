<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bidang extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_bidang');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$get_data=$this->m_bidang->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','nama bidang','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->nama_bidang,
													array('data'=>'<div class="btn-group">
                            <a href="'.site_url('bidang?ubah&id='.$row->id_bidang).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data Bidang"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('bidang/hapus?id='.$row->id_bidang).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'150px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$data['title']='Data Bidang';
		$data['body']=$this->load->view('v_bidang',$databody,true);
		$data['js']=$this->load->view('js/js_bidang',$databody,true);
		$this->load->view('html/html',$data);
	}

	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->m_bidang->insert($data);
		}
		redirect('bidang');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_bidang=$this->input->post('id_bidang');
			$where=array('id_bidang'=>$id_bidang);
			$this->m_bidang->update($data,$where);
		}
		redirect('bidang');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_bidang' => $this->input->get('id'),
		);
		$this->m_bidang->delete($where);
		redirect('bidang');
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_bidang = $this->input->post('id_bidang');
		$nama_bidang = $this->input->post('nama_bidang');
		$data=array(
			'id_bidang' => $id_bidang,
			'nama_bidang' => $nama_bidang,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 12-06-2017:22:27:36  **/
/**************************************/
