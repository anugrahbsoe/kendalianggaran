<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
 {
    parent::__construct();
		checklogged();
		$this->load->model('m_pptk');
		$this->load->model('m_pengguna');
		$this->load->model('m_realisasi');
		$this->load->model('m_anggaran');
	}

	public function index($a='')
	{
		$id_ta=id_ta();
		$databody['id_ta']=$id_ta;
		$data['title']='Beranda';
		$data['crud']='';
		$data['body']=$this->load->view('v_home',$databody,true);
		$data['js']=$this->load->view('js/js_home',$databody,true);
		$this->load->view('html/html',$data);
	}
	public function getgrafik(){
		$id_ta=id_ta();
		$bulan=$this->input->get('bulan');
		$databody['id_ta']=$id_ta;
		$databody['bulan']=$bulan;
		$databody['nama_ta']=nama_ta();
		if($bulan=='' OR $bulan=='semua'){
			$this->load->view('home/v_grafiktahun',$databody);
		}
		else{
			$this->load->view('home/v_grafikbulan',$databody);
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
