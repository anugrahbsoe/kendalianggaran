<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pegawai extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_pegawai');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$get_data=$this->m_pegawai->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','nip pegawai','nama pegawai','no hp','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->nip_pegawai,
													$row->nama_pegawai,
													$row->no_hp,
													array('data'=>'<div class="btn-group">
                            <a href="'.site_url('pegawai?ubah&id='.$row->id_pegawai).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data Pegawai"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('pegawai/hapus?id='.$row->id_pegawai).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'150px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$data['title']='Data Pegawai';
		$data['body']=$this->load->view('v_pegawai',$databody,true);
		$data['js']=$this->load->view('js/js_pegawai',$databody,true);
		$this->load->view('html/html',$data);
	}
				
	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->m_pegawai->insert($data);
		}
		redirect('pegawai');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_pegawai=$this->input->post('id_pegawai');
			$where=array('id_pegawai'=>$id_pegawai);
			$this->m_pegawai->update($data,$where);
		}
		redirect('pegawai');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_pegawai' => $this->input->get('id'),
		);
		$this->m_pegawai->delete($where);
		redirect('pegawai');
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_pegawai = $this->input->post('id_pegawai');
		$nip_pegawai = $this->input->post('nip_pegawai');
		$nama_pegawai = $this->input->post('nama_pegawai');
		$no_hp = $this->input->post('no_hp');
		$data=array(
			'id_pegawai' => $id_pegawai,
			'nip_pegawai' => $nip_pegawai,
			'nama_pegawai' => $nama_pegawai,
			'no_hp' => $no_hp,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 12-06-2017:22:22:18  **/
/**          nasrullah siddik  		   **/
/**************************************/
