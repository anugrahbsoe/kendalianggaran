<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ta extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_ta');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$get_data=$this->m_ta->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','nama ta','aktif','');
		$i=1;
		foreach($get_data->result() as $row){
			if($row->aktif=='Y'){
				$aktif='<a href="#" class="btn btn-default btn-xs" ns-click="true" ns-title="Data Tahun Anggaran"><i class="fa fa-check"></i> Aktifkan</a>';
			}
			else{
				$aktif='<a href="'.site_url('ta/aktif?id='.$row->id_ta).'" class="btn btn-warning btn-xs" ns-click="true"  onclick="return confirm(\'Aktifkan?\');" ns-title="Data Tahun Anggaran"><i class="fa fa-check"></i> Aktifkan</a>';
			}
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->nama_ta,
													$row->aktif,
													array('data'=>'<div class="btn-group">
														'.$aktif.'
                            <a href="'.site_url('ta?ubah&id='.$row->id_ta).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data Tahun Anggaran"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('ta/hapus?id='.$row->id_ta).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'200px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$data['title']='Data Tahun Anggaran';
		$data['body']=$this->load->view('v_ta',$databody,true);
		$data['js']=$this->load->view('js/js_ta',$databody,true);
		$this->load->view('html/html',$data);
	}

	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->m_ta->insert($data);
		}
		$this->_cegahnonaktif();
		redirect('ta');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_ta=$this->input->post('id_ta');
			$where=array('id_ta'=>$id_ta);
			$this->m_ta->update($data,$where);
		}
		$this->_cegahnonaktif();
		redirect('ta');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_ta' => $this->input->get('id'),
		);
		$this->m_ta->delete($where);
		redirect('ta');
	}
	public function aktif(){
		$where=array(
			'id_ta' => $this->input->get('id'),
		);
		$this->m_ta->aktif($where);
		$this->_cegahnonaktif(true,$where);
		redirect('ta');
	}
	private function _cegahnonaktif($param=false,$where=array()){
		if($param==true){
			$this->db->update('tb_ta',array('aktif'=>'T'));
		}
		if($where==null){
			$where=array('nama_ta'=>$nama_ta);
		}
		$cek=$this->db->get_where('tb_ta',array('aktif'=>'Y'));
		if($cek->num_rows()==0){
			$nama_ta=date('Y');
			$cek=$this->db->get_where('tb_ta',$where);
			if($cek->num_rows()>0){
				$this->db->update('tb_ta',array('aktif'=>'Y'),$where);
			}
			else{
				$this->db->limit(1);
				$this->db->update('tb_ta',array('aktif'=>'Y'));
			}
		}
		elseif($cek->num_rows()>1){
			$this->_cegahnonaktif(true);
		}
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_ta = $this->input->post('id_ta');
		$nama_ta = $this->input->post('nama_ta');
		$aktif = $this->input->post('aktif');
		$data=array(
			'id_ta' => $id_ta,
			'nama_ta' => $nama_ta,
			'aktif' => $aktif,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 12-06-2017:22:57:41  **/
/**************************************/
