<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kegiatan extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_kegiatan');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$get_data=$this->m_kegiatan->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','kode kegiatan','nama kegiatan','program','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->kode_program.'. '.$row->kode_kegiatan,
													$row->nama_kegiatan,
													$row->nama_program,
													array('data'=>'<div class="btn-group">
                            <a href="'.site_url('kegiatan?ubah&id='.$row->id_kegiatan).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data Kegiatan"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('kegiatan/hapus?id='.$row->id_kegiatan).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'150px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$data['title']='Data Kegiatan';
		$data['body']=$this->load->view('v_kegiatan',$databody,true);
		$data['js']=$this->load->view('js/js_kegiatan',$databody,true);
		$this->load->view('html/html',$data);
	}

	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->m_kegiatan->insert($data);
		}
		redirect('kegiatan');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_kegiatan=$this->input->post('id_kegiatan');
			$where=array('id_kegiatan'=>$id_kegiatan);
			$this->m_kegiatan->update($data,$where);
		}
		redirect('kegiatan');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_kegiatan' => $this->input->get('id'),
		);
		$this->m_kegiatan->delete($where);
		redirect('kegiatan');
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_kegiatan = $this->input->post('id_kegiatan');
		$kode_kegiatan = $this->input->post('kode_kegiatan');
		$nama_kegiatan = $this->input->post('nama_kegiatan');
		$id_program = $this->input->post('id_program');
		$tanggal_kegiatan = date('Y-m-d');
		$data=array(
			'id_kegiatan' => $id_kegiatan,
			'kode_kegiatan' => $kode_kegiatan,
			'nama_kegiatan' => $nama_kegiatan,
			'id_program' => $id_program,
			'tanggal_kegiatan' => $tanggal_kegiatan,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 12-06-2017:22:26:21  **/
/**************************************/
