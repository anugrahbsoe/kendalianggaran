<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Belanja extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_belanja');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$get_data=$this->m_belanja->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','kode belanja','nama belanja','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->kode_belanja,
													$row->nama_belanja,
													array('data'=>'<div class="btn-group">
                            <a href="'.site_url('belanja?ubah&id='.$row->id_belanja).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data Master Belanja"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('belanja/hapus?id='.$row->id_belanja).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'150px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$data['title']='Data Master Belanja';
		$data['body']=$this->load->view('v_belanja',$databody,true);
		$data['js']=$this->load->view('js/js_belanja',$databody,true);
		$this->load->view('html/html',$data);
	}

	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->m_belanja->insert($data);
		}
		redirect('belanja');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_belanja=$this->input->post('id_belanja');
			$where=array('id_belanja'=>$id_belanja);
			$this->m_belanja->update($data,$where);
		}
		redirect('belanja');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_belanja' => $this->input->get('id'),
		);
		$this->m_belanja->delete($where);
		redirect('belanja');
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_belanja = $this->input->post('id_belanja');
		$kode_belanja = $this->input->post('kode_belanja');
		$nama_belanja = $this->input->post('nama_belanja');
		$tanggal_belanja = date('Y-m-d');
		$data=array(
			'id_belanja' => $id_belanja,
			'kode_belanja' => $kode_belanja,
			'nama_belanja' => $nama_belanja,
			'tanggal_belanja' => $tanggal_belanja,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 28-06-2017:00:32:16  **/
/**          nasrullah siddik  		   **/
/**************************************/
