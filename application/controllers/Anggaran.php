<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Anggaran extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_anggaran');
			$this->load->model('m_kegiatan');
			$this->load->model('m_pptk');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$id_ta = id_ta();
		$id_pegawai=$this->input->get('id_pegawai');
		if($id_pegawai!=''){
			$this->session->set_userdata('id_pegawai',$id_pegawai);
		}
		elseif($this->session->userdata('id_pegawai')!=''){
			$id_pegawai=$this->session->userdata('id_pegawai');
		}
		$this->db->where('a.id_pegawai',$id_pegawai);
		$get_data=$this->m_anggaran->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','nip pegawai','nama','kode rekening','kegiatan','uraian','pagu anggaran','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->nip_pegawai,
													$row->nama_pegawai,
													$row->kode_program.'.'.$row->kode_kegiatan.'.'.$row->kode_belanja,
													$row->nama_kegiatan,
													$row->nama_belanja,
													uangindonesia($row->pagu_anggaran,'Rp.'),
													array('data'=>'<div class="btn-group">
                            <a href="'.site_url('anggaran?ubah&id='.$row->id_anggaran).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data Anggaran"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('anggaran/hapus?id='.$row->id_anggaran).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'150px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$databody['id_pegawai']=$id_pegawai;
		$databody['id_ta']=$id_ta;
		$data['title']='Data Anggaran';
		$data['body']=$this->load->view('v_anggaran',$databody,true);
		$data['js']=$this->load->view('js/js_anggaran',$databody,true);
		$this->load->view('html/html',$data);
	}

	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->m_anggaran->insert($data);
		}
		redirect('anggaran');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_anggaran=$this->input->post('id_anggaran');
			$where=array('id_anggaran'=>$id_anggaran);
			$this->m_anggaran->update($data,$where);
		}
		redirect('anggaran');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_anggaran' => $this->input->get('id'),
		);
		$this->m_anggaran->delete($where);
		redirect('anggaran');
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_anggaran = $this->input->post('id_anggaran');
		$id_pegawai = $this->input->post('id_pegawai');
		$id_kegiatan = $this->input->post('id_kegiatan');
		$id_ta = $this->input->post('id_ta');
		$id_belanja = $this->input->post('id_belanja');
		$pagu_anggaran = $this->input->post('pagu_anggaran');
		$tanggal_anggaran = date('Y-m-d');
		$data=array(
			'id_anggaran' => $id_anggaran,
			'id_pegawai' => $id_pegawai,
			'id_kegiatan' => $id_kegiatan,
			'id_ta' => $id_ta,
			'id_belanja' => $id_belanja,
			'pagu_anggaran' => $pagu_anggaran,
			'tanggal_anggaran' => $tanggal_anggaran,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 12-06-2017:23:07:34  **/
/**************************************/
