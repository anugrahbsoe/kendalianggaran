<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengguna extends CI_Controller {

	##################################
	##          CONSTRUCT           ##
	##################################
	public function __construct()
     {
			parent::__construct();
			$this->load->model('m_pengguna');
}

	##################################
	##          VIEW DEFAULT        ##
	##################################
	public function index(){
		$get_data=$this->m_pengguna->get_data();
		$template = array(
			'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped dt-responsive display" id="table">',
		);
		$this->table->set_template($template);
		$this->table->set_heading('No','username','password','nama','nip pegawai','bidang','level','');
		$i=1;
		foreach($get_data->result() as $row){
			$this->table->add_row(array('data'=>$i,'width'=>'50px','align'=>'center'),
													$row->username,
													$row->password,
													$row->nama,
													$row->nama_pegawai,
													$row->nama_bidang,
													$row->level,
													array('data'=>'<div class="btn-group">
                            <a href="'.site_url('pengguna?ubah&id='.$row->id_pengguna).'" class="btn btn-info btn-xs" ns-click="true" ns-title="Data Pengguna"><i class="fa fa-edit"></i> Ubah</a>
                            <a href="'.site_url('pengguna/hapus?id='.$row->id_pengguna).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Yakin menghapus data?\');"><i class="fa fa-trash-o"></i> Hapus</a>
                        </div>','width'=>'150px'));
			$i++;
		}
		$databody['table']=$this->table->generate();
		$data['title']='Data Pengguna';
		$data['body']=$this->load->view('v_pengguna',$databody,true);
		$data['js']=$this->load->view('js/js_pengguna',$databody,true);
		$this->load->view('html/html',$data);
	}

	##################################
	##            TAMBAH            ##
	##################################
	public function tambah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$this->m_pengguna->insert($data);
		}
		redirect('pengguna');
	}
	##################################
	##            UBAH              ##
	##################################
	public function ubah(){
		if($this->input->post('simpan')){
			$data=$this->_datapost();
			$id_pengguna=$this->input->post('id_pengguna');
			$where=array('id_pengguna'=>$id_pengguna);
			$this->m_pengguna->update($data,$where);
		}
		redirect('pengguna');
	}
	##################################
	##            HAPUS             ##
	##################################
	public function hapus(){
		$where=array(
			'id_pengguna' => $this->input->get('id'),
		);
		$this->m_pengguna->delete($where);
		redirect('pengguna');
	}
	##################################
	##      KUMPULKAN NILAI POST    ##
	##################################
	private function _datapost(){
		$id_pengguna = $this->input->post('id_pengguna');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$nama = $this->input->post('nama');
		$id_pegawai = $this->input->post('id_pegawai');
		$level = $this->input->post('level');
		$id_bidang = $this->input->post('id_bidang');
		$data=array(
			'id_pengguna' => $id_pengguna,
			'username' => $username,
			'password' => $password,
			'nama' => $nama,
			'id_pegawai' => $id_pegawai,
			'level' => $level,
			'id_bidang' => $id_bidang,
		);
		return $data;
	}
	//end class
}



/**************************************/
/**  created on 12-06-2017:22:29:54  **/
/**************************************/
