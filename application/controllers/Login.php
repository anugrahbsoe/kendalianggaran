  <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
       {
            parent::__construct();
       }

	public function index($a="",$b="")
	{
    checklogged();

		$crud['title']="Masuk";
		$crud['action']="login";

		$action=$crud['action'];

    $username=$this->input->post('username');
    $password=$this->input->post('password');
		//other configuration
		if(isset($_POST['login'])){
			if($username!='' AND $password!=''){
			  $cek=$this->db->get_where('tb_pengguna',array('username'=>$username,'password'=>$password));
        if($cek->num_rows()>0){
            $row=$cek->row();
            $this->session->set_userdata('logged',"ya");
            $this->session->set_userdata('nama',$row->nama);
            $this->session->set_userdata('level',$row->level);
            $this->session->set_userdata('username',$row->username);
            $this->session->set_userdata('id_pengguna',$row->id_pengguna);
            $this->session->set_userdata('id_pegawai',$row->id_pegawai);
            $this->session->set_userdata('id_bidang',$row->id_bidang);
            redirect();
        }
        else{
    			$this->session->set_flashdata('info',info_danger(icon('times').' Nama Pengguna atau Password Salah'));
    			redirect($action);
        }
			}
			else{
				$this->session->set_flashdata('info',info_danger(icon('times').' Gagal Masuk, Nama Pengguna Kosong'));
				redirect($action);
			}
		}
		else{
			$this->load->view('v_login',$crud);
		}

	}
	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
}
