<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pegawai extends CI_Model {
	function get_data(){
			$data=$this->db->select('*')
						->from('tb_pegawai')
						->order_by('id_pegawai','DESC')
						->get();
					return $data;
	}
	function insert($data){
		$this->db->insert('tb_pegawai',$data);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
	}
	function update($data,$where){
		$cek=$this->db->get_where('tb_pegawai',$where);
		if($cek->num_rows()>0){
			$this->db->update('tb_pegawai',$data,$where);
			$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
		}
		else{
			$this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
		}
	}
	function delete($where){
		$this->db->delete('tb_pegawai',$where);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
	}
}
