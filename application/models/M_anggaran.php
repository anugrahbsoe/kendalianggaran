<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_anggaran extends CI_Model {
	function get_data(){
				$data=$this->db->select('a.*,b.nip_pegawai,b.nama_pegawai,c.kode_kegiatan,c.nama_kegiatan,f.kode_program,f.nama_program,d.nama_ta,e.kode_belanja,e.nama_belanja')
						->from('tb_anggaran a')
						->join('tb_pegawai b','a.id_pegawai=b.id_pegawai','left')
						->join('tb_kegiatan c','a.id_kegiatan=c.id_kegiatan','left')
						->join('tb_ta d','a.id_ta=d.id_ta','left')
						->join('tb_belanja e','a.id_belanja=e.id_belanja','left')
						->join('tb_program f','c.id_program=f.id_program','left')
						->order_by('id_anggaran','DESC')
						->get();
				return $data;
	}
	function get_pagu($fetch=true){
				$data=$this->db->select('IFNULL(SUM(pagu_anggaran),0) total_pagu')
						->from('tb_anggaran a')
						->join('tb_pegawai b','a.id_pegawai=b.id_pegawai','left')
						->join('tb_kegiatan c','a.id_kegiatan=c.id_kegiatan','left')
						->join('tb_ta d','a.id_ta=d.id_ta','left')
						->join('tb_belanja e','a.id_belanja=e.id_belanja','left')
						->join('tb_program f','c.id_program=f.id_program','left')

						->join('tb_pegawai ba','b.id_pegawai=ba.id_pegawai','left')
						->join('tb_pptk baa','ba.id_pegawai=baa.id_pegawai','left')
						->where('a.id_ta = baa.id_ta')

						->order_by('id_anggaran','DESC')
						->get();
				if($fetch==true){
					$r=$data->row();
					return $r->total_pagu;
				}
				else{
					return $data;
				}
	}
	function insert($data){
		$this->db->insert('tb_anggaran',$data);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
	}
	function update($data,$where){
		$cek=$this->db->get_where('tb_anggaran',$where);
		if($cek->num_rows()>0){
			$this->db->update('tb_anggaran',$data,$where);
			$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
		}
		else{
			$this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
		}
	}
	function delete($where){
		$this->db->delete('tb_anggaran',$where);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
	}
	
}
