<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_kegiatan extends CI_Model {
	function get_data(){
				$data=$this->db->select('a.*,b.kode_program,b.nama_program')
						->from('tb_kegiatan a')
						->join('tb_program b','a.id_program=b.id_program','left')
						->order_by('id_kegiatan','DESC')
						->get();
				return $data;
	}
	function insert($data){
		$this->db->insert('tb_kegiatan',$data);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
	}
	function update($data,$where){
		$cek=$this->db->get_where('tb_kegiatan',$where);
		if($cek->num_rows()>0){
			$this->db->update('tb_kegiatan',$data,$where);
			$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
		}
		else{
			$this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
		}
	}
	function delete($where){
		$this->db->delete('tb_kegiatan',$where);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
	}

	function printcoba(){
		$data=$this->db->select('a.*, b.id_kegiatan, b.pagu_anggaran AS pagu_anggaran, SUM(b.pagu_anggaran) AS total_pagu_anggaran, c.kode_program, c.nama_program, d.id_anggaran, d.realisasi AS realisasi, SUM(d.realisasi) AS total_realisasi, e.*')
				->from('tb_kegiatan a')
				->join('tb_anggaran b','a.id_kegiatan = b.id_kegiatan','left')
				->join('tb_program c','a.id_program=c.id_program','left')
				->join('tb_realisasi d','b.id_anggaran=d.id_anggaran','left')
				->join('tb_belanja e','b.id_belanja=e.id_belanja','left')
				->get();
		return $data;
	}

		public function printkegiatan(){

			$sql = "SELECT * FROM tb_kegiatan
					LEFT JOIN tb_anggaran ON tb_kegiatan.id_kegiatan = tb_anggaran.id_kegiatan
					LEFT JOIN tb_program ON tb_kegiatan.id_program = tb_program.id_program
					LEFT JOIN tb_realisasi ON tb_anggaran.id_anggaran = tb_realisasi.id_anggaran
					LEFT JOIN tb_belanja ON tb_anggaran.id_belanja = tb_belanja.id_belanja
					WHERE kode_kegiatan =  kode_kegiatan" ;
			$query = $this->db->query($sql);

			if($query->num_rows()>0)
				{
				return $query-> result_array();
			}else{
				return array();
			}
		}
}
