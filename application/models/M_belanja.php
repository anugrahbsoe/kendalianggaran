<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_belanja extends CI_Model {
	function get_data(){
			$data=$this->db->select('*')
						->from('tb_belanja')
						->order_by('id_belanja','DESC')
						->get();
					return $data;
	}
	function insert($data){
		$this->db->insert('tb_belanja',$data);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
	}
	function update($data,$where){
		$cek=$this->db->get_where('tb_belanja',$where);
		if($cek->num_rows()>0){
			$this->db->update('tb_belanja',$data,$where);
			$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
		}
		else{
			$this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
		}
	}
	function delete($where){
		$this->db->delete('tb_belanja',$where);
		$this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
	}
}
