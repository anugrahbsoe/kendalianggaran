<?php
  function th($a){
    return '<th>'.$a.'</th>';
  }
  function td($a){
    return '<td>'.$a.'</td>';
  }
  function bkpsdm($a){
    return '050';
  }
  function checklogged($level=''){
    $nfw=&get_instance();
    $uri=$nfw->uri->segment(1);
    $logged=$nfw->session->userdata('logged');
    if($uri!='login'){
      $nfw->session->set_userdata('terkahir_akses',current_url());
      if($logged!='ya'){
        redirect('login');
      }
      else{
        if(is_numeric($level)){
          $lvl=$nfw->session->userdata('level');
          if($lvl!=$level){
            redirect();
          }
        }
      }
    }
    else{
      if($logged=='ya'){
        redirect();
      }
    }
  }

  function nama_ta($id_ta=''){
    $nfw=&get_instance();
    if($id_ta!=''){
      $nfw->db->where('id_ta',$id_ta);
    }
    else{
      $nfw->db->where('aktif','Y');
    }
    $getta=$nfw->db->get("tb_ta");
    if($getta->num_rows()>0){
      $r=$getta->row();
      $nama_ta=$r->nama_ta;
    }
    else{
      $nama_ta='Tidak Ditemukan';
    }
    return $nama_ta;
  }

  function id_ta(){
    $nfw=&get_instance();
    $nfw->db->where('aktif','Y');
    $getta=$nfw->db->get("tb_ta");
    if($getta->num_rows()>0){
      $r=$getta->row();
      $id_ta=$r->id_ta;
      return $id_ta;
    }
    else{
      redirect('ta');
    }
  }

  function level(){
    $nfw=&get_instance();
    $level=$nfw->session->userdata("level");
    return $level;
  }

  function nama_level($a=''){
    if($a==''){
      $level=level();
    }
    else{
      $level=$a;
    }
    if($a==0){
      $nama_level='Administrator';
    }
    elseif($a==1){
      $nama_level='Puskesmas';
    }
    return $nama_level;
  }

  function logged(){
    $nfw=&get_instance();
    $level=$nfw->session->userdata("logged");
    return $level;
  }

  function kd_puskes(){
    $nfw=&get_instance();
    $level=$nfw->session->userdata("kd_puskes");
    return $level;
  }

  function id_pengguna(){
    $nfw=&get_instance();
    return $nfw->session->userdata("id_pengguna");
  }
  function kd_kabupaten(){
    $nfw=&get_instance();
    return $nfw->session->userdata("kd_kabupaten");
  }

  function nama_kabupaten($kd_kabupaten){
    $nfw=&get_instance();
    $row=$nfw->db->get_where("tkabupaten",array("kd_kabupaten"=>$kd_kabupaten))->row();
    return $row->nama_kabupaten;
  }

  function nama_tingkat($kd_tingkat){
    $nfw=&get_instance();
    $row=$nfw->db->get_where("ttingkat",array("kd_tingkat"=>$kd_tingkat))->row();
    return $row->nama_tingkat;
  }

  function nama_jkomoditas($kd_jkomoditas){
    $nfw=&get_instance();
    $row=$nfw->db->get_where("tjkomoditas",array("kd_jkomoditas"=>$kd_jkomoditas))->row();
    return $row->nama_jkomoditas;
  }

  function nama_jsatuan($kd_jkomoditas){
    $nfw=&get_instance();
    $row=$nfw->db->query("SELECT * FROM tjkomoditas a LEFT JOIN tsatuan b ON a.kd_satuan=b.kd_satuan WHERE a.kd_jkomoditas='".$kd_jkomoditas."'")->row();
    return $row->nama_satuan;
  }

  function assets($a=''){
    return base_url().'assets/'.$a;
  }

  if (!function_exists('is_valid_date')) {
   function is_valid_date($str) {
      $split = [];
      if (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $str, $split)) {
         return checkdate($split[2], $split[3], $split[1]);
      }
      return false;
   }
}

if (!function_exists('bulan')) {
   function bulan($kode, $type = 'L') {
      $bulan = '';
      switch ($kode) {
         case '01':
            $bulan = 'Januari';
            break;
         case '02':
            $bulan = 'Februari';
            break;
         case '03':
            $bulan = 'Maret';
            break;
         case '04':
            $bulan = 'April';
            break;
         case '05':
            $bulan = 'Mei';
            break;
         case '06':
            $bulan = 'Juni';
            break;
         case '07':
            $bulan = 'Juli';
            break;
         case '08':
            $bulan = 'Agustus';
            break;
         case '09':
            $bulan = 'September';
            break;
         case '10':
            $bulan = 'Oktober';
            break;
         case '11':
            $bulan = 'Nopember';
            break;
         case '12':
            $bulan = 'Desember';
            break;
      }
      if ($type != 'L') {
         return substr($bulan, 0, 3);
      }
      return $bulan;
   }
}

  function indo_date($str) {
      if (!is_valid_date($str)) return NULL;
      $exp = explode("-", $str);
      return $exp[2] . ' ' . bulan($exp[1]) . ' ' . $exp[0];
   }
 ?>
