<script type="text/javascript">
  $("[name=kd_komoditas]").change(getjkomoditas);
  getjkomoditas();
  function getjkomoditas(){
    $kd_komoditas=$("[name=kd_komoditas]").val();
    $def_kd_jkomoditas=$("[name=def_kd_jkomoditas]").val();
    if($kd_komoditas!=''){
      $.ajax({
        url:"<?=site_url('ajax/getjkomoditas')?>/"+$kd_komoditas+"?def_kd_jkomoditas="+$def_kd_jkomoditas,
        success:function(data){
          $(".load-tjkomoditas").html(data);
        }
      })
    }
  }
</script>
<script type="text/javascript">
  $("[name=jenisdata]").change(function(){
    $val=$(this).val();
    if($val=='harian'){
      $("[name=minggu]").attr({"disabled":"disabled"});
      $("[name=tanggal],[name=bulan]").removeAttr("disabled");
    }
    else if($val=='mingguan'){
      $("[name=minggu]").removeAttr("disabled");
      $("[name=tanggal]").attr({"disabled":"disabled"});
    }
    else if($val=='bulanan'){
      $("[name=tanggal],[name=minggu]").attr({"disabled":"disabled"});
      $("[name=bulan]").removeAttr("disabled");

    }
    else{
      $("[name=tanggal],[name=minggu],[name=bulan]").attr({"disabled":"disabled"});

    }
  })
</script>

<?php
if($jenisdata=='harian'){
  $this->load->view('grafik/v_harian.php');
}
elseif($jenisdata=='mingguan'){
  $this->load->view('grafik/v_mingguan.php');
}
elseif($jenisdata=='bulanan'){
  $this->load->view('grafik/v_bulanan.php');
}
else{
  $this->load->view('grafik/v_tahunan.php');

}
 ?>
