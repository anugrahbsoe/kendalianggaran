<script type="text/javascript">
  $("[name=jenisdata]").change(function(){
    $val=$(this).val();
    if($val=='harian'){
      $("[name=minggu]").attr({"readonly":"readonly"});
      $("[name=tanggal],[name=bulan]").removeAttr("readonly");
    }
    else if($val=='mingguan'){
      $("[name=minggu]").removeAttr("readonly");
      $("[name=tanggal]").attr({"readonly":"readonly"});
    }
    else if($val=='bulanan'){
      $("[name=tanggal],[name=minggu]").attr({"readonly":"readonly"});
      $("[name=bulan]").removeAttr("readonly");

    }
    else{
      $("[name=tanggal],[name=minggu],[name=bulan]").attr({"readonly":"readonly"});

    }
  })
</script>
