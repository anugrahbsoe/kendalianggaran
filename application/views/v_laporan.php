  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Kegiatan</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Kegiatan</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="row">
              <form action="<?=site_url()?>laporan" type="get">
               <div class="col-md-4">
             <div class="form-group  full">
               <?php
                 $op=NULL;
                 $op['']='Pilih PPTK';
                 $this->db->where('a.id_ta',$id_ta);
                 $data=$this->m_pptk->get_data();
                 foreach($data->result() as $row){
                   $op[$row->id_pegawai]=$row->nip_pegawai.'-'.$row->nama_pegawai;
                 }
                 echo select('id_pegawai',$op,$id_pegawai,'','required data-md-selectize');?>
             </div>
               </div>
               <div class="col-md-4">
             <div class="form-group">
             <button type="submit" class="btn btn-info">Tampil</button>
             </div>
               </div>
              </form>
              </div>
              <hr>
              <?php echo $this->session->flashdata('info');?>
              <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
