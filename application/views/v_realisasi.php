<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Realisasi</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Realisasi</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_realisasi' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_realisasi',$where)->row();
                $id_realisasi = $row->id_realisasi;
								$id_anggaran = $row->id_anggaran;
								$id_ta = $row->id_ta;
								$realisasi = $row->realisasi;
								$tanggal_realisasi = $row->tanggal_realisasi;
								$status = $row->status;
              }
              else{
                $parameter='tambah';
                $id_realisasi = '';
								$id_anggaran = '';
								$realisasi = '';
								$tanggal_realisasi = date('Y-m-d');
								$status = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>realisasi/<?=$parameter?>">
						<?php echo input_hidden('id_realisasi',$id_realisasi,'','required');?>
            <div class="col-lg-12">
							<div class="form-group">
								<label>PPTK</label>
								<?php
									$op=NULL;
									$this->db->order_by('nip_pegawai','ASC');
									$data=$this->db->get_where('tb_pegawai',array('id_pegawai'=>$id_pegawai));
									foreach($data->result() as $row){
										$op[$row->id_pegawai]=$row->nama_pegawai.' (NIP. '.$row->nip_pegawai.')';
									}
									echo select('id_pegawai',$op,$id_pegawai,'','required data-md-selectize');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>nama anggaran</label>
								<?php
									$op=NULL;
									$op['']='Pilih Salah Satu';
                  $this->db->where('a.id_ta',$id_ta);
                  $this->db->where('b.id_pegawai',$id_pegawai);
									$data=$this->m_anggaran->get_data();
									foreach($data->result() as $row){
										$op[$row->id_anggaran]=$row->kode_belanja.' - '.$row->nama_belanja.' - '.$row->nama_kegiatan;
									}
									echo select('id_anggaran',$op,$id_anggaran,'','required data-md-selectize');?>
							</div>
						</div>


						<div class="col-lg-12">
							<div class="form-group">
								<label>realisasi</label>
								<?php echo input_text('realisasi',$realisasi,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>tanggal realisasi</label>
								<?php echo input_date('tanggal_realisasi',$tanggal_realisasi,'md-input','required');?>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="form-group">
								<label>status</label>
								<?php
									$op=NULL;
									$op['']='Pilih Salah Satu';
										$op['Realisasi']='Realisasi';
										$op['Talangan']='Talangan';
									echo select('status',$op,$status,'','required data-md-selectize');?>
							</div>
						</div>
            <div class="col-lg-12">
							<div class="form-group">
								<label>tahun anggaran</label>
								<?php
									$op=NULL;
									$op['']='Pilih Salah Satu';
									$this->db->order_by('nama_ta','ASC');
									$data=$this->db->get('tb_ta');
									foreach($data->result() as $row){
										$op[$row->id_ta]=$row->nama_ta;
									}
									echo select('id_ta',$op,$id_ta,'','required data-md-selectize');?>
							</div>
						</div>
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Realisasi</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Realisasi</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">
        <div class="row">
         <form action="<?=site_url()?>realisasi" type="get">
          <div class="col-md-4">
        <div class="form-group  full">
          <?php
            $op=NULL;
            $op['']='Pilih PPTK';
            $this->db->where('a.id_ta',$id_ta);
            $data=$this->m_pptk->get_data();
            foreach($data->result() as $row){
              $op[$row->id_pegawai]=$row->nip_pegawai.'-'.$row->nama_pegawai;
            }
            echo select('id_pegawai',$op,$id_pegawai,'','required data-md-selectize');?>
        </div>
          </div>
          <div class="col-md-4">
        <div class="form-group">
        <button type="submit" class="btn btn-info">Tampil</button>
        </div>
          </div>
         </form>
         </div>
         <hr>
        <a href="<?=site_url()?>realisasi?tambah" class="btn btn-success" ns-click="true" ns-title="Data Realisasi"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->
