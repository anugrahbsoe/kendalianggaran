<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Bidang</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Bidang</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_bidang' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_bidang',$where)->row();
                $id_bidang = $row->id_bidang;
								$nama_bidang = $row->nama_bidang;
              }
              else{
                $parameter='tambah';
                $id_bidang = '';
								$nama_bidang = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>bidang/<?=$parameter?>">
						<?php echo input_hidden('id_bidang',$id_bidang,'','required');?>
						<div class="col-lg-6 full">
							<div class="form-group">
								<label>nama bidang</label>
								<?php echo textarea('nama_bidang',$nama_bidang,'md-input','required');?>
							</div>
						</div>
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Bidang</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Bidang</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

        <a href="<?=site_url()?>bidang?tambah" class="btn btn-success" ns-click="true" ns-title="Data Bidang"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->
