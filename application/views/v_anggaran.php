<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Anggaran</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Anggaran</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_anggaran' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_anggaran',$where)->row();
                $id_anggaran = $row->id_anggaran;
								$id_pegawai = $row->id_pegawai;
								$id_kegiatan = $row->id_kegiatan;
								$id_ta = $row->id_ta;
                $id_belanja=$row->id_belanja;
								$pagu_anggaran = $row->pagu_anggaran;
              }
              else{
                $parameter='tambah';
                $id_anggaran = '';
								$id_kegiatan = '';
                $id_belanja='';
								$pagu_anggaran = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>anggaran/<?=$parameter?>">
						<?php echo input_hidden('id_anggaran',$id_anggaran,'','required');?>
						<div class="col-lg-12">
							<div class="form-group">
								<label>PPTK</label>
								<?php
									$op=NULL;
									$op['']='Pilih Salah Satu';
                  $this->db->order_by('nip_pegawai','ASC');
                  $this->db->where('a.id_ta',$id_ta);
									$this->db->where('a.id_pegawai',$id_pegawai);
									$data=$this->m_pptk->get_data();
									foreach($data->result() as $row){
										$op[$row->id_pegawai]=$row->nama_pegawai.' (NIP. '.$row->nip_pegawai.')';
									}
									echo select('id_pegawai',$op,$id_pegawai,'','required data-md-selectize');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>kegiatan</label>
								<?php
									$op=NULL;
									$op['']='Pilih Salah Satu';
									$this->db->order_by('kode_kegiatan','ASC');
                  if($parameter=='ubah'){
                    $this->db->where('id_kegiatan NOT IN (SELECT id_kegiatan FROM tb_anggaran WHERE id_ta='.$id_ta.' AND id_kegiatan!='.$id_kegiatan.' AND id_pegawai='.$id_pegawai.')');
                  }
                  else{
                    $this->db->where('id_kegiatan NOT IN (SELECT id_kegiatan FROM tb_anggaran WHERE id_ta='.$id_ta.' AND id_pegawai!='.$id_pegawai.')');
                  }
									$data=$this->m_kegiatan->get_data('tb_kegiatan');
									foreach($data->result() as $row){
                    $op[$row->id_kegiatan]=$row->kode_program.'. '.$row->kode_kegiatan.' - '.$row->nama_kegiatan;
									}
									echo select('id_kegiatan',$op,$id_kegiatan,'','required data-md-selectize');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Belanja</label>
								<?php
									$op=NULL;
									$op['']='Pilih Salah Satu';
									$this->db->order_by('nama_belanja','ASC');
									$data=$this->db->get('tb_belanja');
									foreach($data->result() as $row){
										$op[$row->id_belanja]=$row->kode_belanja.' - '.$row->nama_belanja;
									}
									echo select('id_belanja',$op,$id_belanja,'','required data-md-selectize');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>pagu anggaran</label>
								<?php echo input_number('pagu_anggaran',$pagu_anggaran,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Tahun Anggaran</label>
								<?php
									$op=NULL;
									$op['']='Pilih Salah Satu';
									$this->db->order_by('nama_ta','ASC');
									$data=$this->db->get('tb_ta');
									foreach($data->result() as $row){
										$op[$row->id_ta]=$row->nama_ta;
									}
									echo select('id_ta',$op,$id_ta,'','required data-md-selectize');?>
							</div>
						</div>
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Anggaran</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Anggaran</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
         <div class="row">
         <form action="<?=site_url()?>anggaran" type="get">
         	<div class="col-md-4">
				<div class="form-group  full">
					<?php
						$op=NULL;
            $op['']='Pilih PPTK';
            $this->db->where('a.id_ta',$id_ta);
            $data=$this->m_pptk->get_data();
						foreach($data->result() as $row){
							$op[$row->id_pegawai]=$row->nip_pegawai.'-'.$row->nama_pegawai;
						}
						echo select('id_pegawai',$op,$id_pegawai,'','required data-md-selectize');?>
				</div>
         	</div>
         	<div class="col-md-4">
				<div class="form-group">
				<button type="submit" class="btn btn-info">Tampil</button>
				</div>
         	</div>
         </form>
         </div>
         <hr>
        <a href="<?=site_url()?>anggaran?tambah" class="btn btn-success" ns-click="true" ns-title="Data Anggaran"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->
