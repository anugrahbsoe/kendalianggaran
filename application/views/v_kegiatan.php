<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Kegiatan</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Kegiatan</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_kegiatan' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_kegiatan',$where)->row();
                $id_kegiatan = $row->id_kegiatan;
								$kode_kegiatan = $row->kode_kegiatan;
								$nama_kegiatan = $row->nama_kegiatan;
								$id_program = $row->id_program;
								$tanggal_kegiatan = $row->tanggal_kegiatan;
              }
              else{
                $parameter='tambah';
                $id_kegiatan = '';
								$kode_kegiatan = '';
								$nama_kegiatan = '';
								$id_program = '';
								$tanggal_kegiatan = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>kegiatan/<?=$parameter?>">
						<?php echo input_hidden('id_kegiatan',$id_kegiatan,'','required');?>
						<div class="col-lg-12">
							<div class="form-group">
								<label>kode kegiatan</label>
								<?php echo input_text('kode_kegiatan',$kode_kegiatan,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-6 full">
							<div class="form-group">
								<label>nama kegiatan</label>
								<?php echo textarea('nama_kegiatan',$nama_kegiatan,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>nama program</label>
								<?php
									$op=NULL;
									$op['']='Pilih Salah Satu';
									$this->db->order_by('kode_program','ASC');
									$data=$this->db->get('tb_program');
									foreach($data->result() as $row){
										$op[$row->id_program]=$row->kode_program.' - '.$row->nama_program;
									}
									echo select('id_program',$op,$id_program,'','required data-md-selectize');?>
							</div>
						</div>
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Kegiatan</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Kegiatan</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

        <a href="<?=site_url()?>kegiatan?tambah" class="btn btn-success" ns-click="true" ns-title="Data Kegiatan"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->
