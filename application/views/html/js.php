<!-- jQuery -->
<script src="<?=assets()?>vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?=assets()?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?=assets()?>vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?=assets()?>vendors/nprogress/nprogress.js"></script>

<!-- Custom Theme Scripts -->

<script src="<?=assets()?>src/js/jquery.validate.min.js"></script>


<script src="<?=assets()?>vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->


<script src="<?php echo base_url()?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

<script src="<?=assets()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=assets()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?=assets()?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=assets()?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?=assets()?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?=assets()?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=assets()?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?=assets()?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?=assets()?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<!-- <script src="<?=assets()?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script> -->
<!-- <script src="<?=assets()?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script> -->
<script src="<?=assets()?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

<script src="<?=assets()?>vendors/jszip/dist/jszip.min.js"></script>
<script src="<?=assets()?>vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?=assets()?>vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="<?=assets()?>build/js/custom.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  $('#table').DataTable({
        "aLengthMenu":[10,15,20,30,50,100],
        "bStateSave": true,
         "oLanguage":
         { "sLengthMenu": "Tampilkan _MENU_ Data",
          "sZeroRecords": "Maaf data tidak ditemukan",
          "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
          "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
           "sInfoFiltered": "(Disaring dari _MAX_ total data)",
           "sSearch": "Cari:",
         },
     responsive: true }
   );
});
$(".wraplist li a[href='<?=site_url($this->uri->segment(1))?>']").addClass("active");
$(".wraplist li a[href='<?=site_url($this->uri->segment(1))?>']").parent().parent().parent().addClass("open");
$(".validate").validate();
function disabling(){ $("[name=simpan]").attr({"disabled":"disabled"}); }
function cekduplikat($val,$table,$where,$def,$text,$name,$setname){ $("[name="+$name+"]").parent().after().append("<small class=check style=color:#009>Memvalidasi data.....</small>");$sdef=$def; if($setname!=undefined){ $val=$("[name="+$setname+"]").val(); } if($def==''){ $def='null'; } if($where!=$def){ $.ajax({ url:"<?=site_url()?>ajax/checkduplicate", data:"val="+$val+ "&table="+$table+ "&where="+$where+ "&def="+$def, type:"GET", success:function(data){ $(".check").remove(); if(data==1){ $("[name="+$name+"]").parent().after().append("<small class=check style=color:#b22>"+$text+" Sudah ada...</small>"); setTimeout(function(){ $(".check").remove(); },2000); if($setname!=undefined){ $("[name="+$setname+"]").val($sdef); } $("[name="+$name+"]").val($sdef); } else{ $("[name="+$name+"]").parent().after().append("<small class=check style=color:#090>"+$text+" Dapat digunakan...</small>"); setTimeout(function(){ $(".check").remove(); },2000); $("[name=simpan]").removeAttr("disabled"); } } }) } }
function getkey(e) { if (window.event) return window.event.keyCode; else if (e) return e.which; else return null; } function goodchars(e, goods, field){ var key, keychar; key = getkey(e); if (key == null) return true; keychar = String.fromCharCode(key); keychar = keychar.toLowerCase(); goods = goods.toLowerCase(); if (goods.indexOf(keychar) != -1) return true; if ( key==null || key==0 || key==8 || key==9 || key==27 ) return true; if (key == 13) { var i; for (i = 0; i < field.form.elements.length; i++) if (field == field.form.elements[i]) break; i = (i + 1) % field.form.elements.length; field.form.elements[i].focus(); return false; }; return false; }

</script>
