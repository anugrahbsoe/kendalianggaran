<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- Bootstrap -->

<link href="<?=assets()?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="<?=assets()?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="<?=assets()?>vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- Datatables -->
<link href="<?=assets()?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<link href="<?=assets()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?=assets()?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?=assets()?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?=assets()?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?=assets()?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="<?=assets()?>build/css/custom.min.css" rel="stylesheet">
<style media="screen">
  input,select,textarea{
    width: auto !important;
  }
  .full input,.full select,.full textarea{
  	width: 100% !important
  }
  .text-gray{
    color:#aaa
  }
  .text-orange{
    color: #f80
  }
  input.valid{
    border: 1px solid #0b0;
    background: #efe
  }
  input.error{
    color: #c22;
    background: #fee;
    border: 1px solid #c22
  }
  label.error{
    color: #c22;
    font-style: italic;
  }
  .alert{
    border-radius: 0 !important
  }
  /*.left_col{
    background: url('<?=base_url('assets/images/backmenu.jpg')?>')
  }
  .nav.child_menu>li>a, .nav.side-menu>li>a{
    color: #333
  }*/
</style>
