<?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
      <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Tahun Anggaran</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Tahun Anggaran</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <?php
              if(isset($_GET['ubah'])){
                $parameter='ubah';
                $where=array(
                	'id_ta' => $this->input->get('id'),
                );
                $row=$this->db->get_where('tb_ta',$where)->row();
                $id_ta = $row->id_ta;
								$nama_ta = $row->nama_ta;
								$aktif = $row->aktif;
              }
              else{
                $parameter='tambah';
                $id_ta = '';
								$nama_ta = '';
								$aktif = '';
              }
            ?>
           <form class="validate form-horizontal" method="POST" action="<?=site_url()?>ta/<?=$parameter?>">                
						<?php echo input_hidden('id_ta',$id_ta,'','required');?>
						<div class="col-lg-12">
							<div class="form-group">
								<label>nama ta</label>
								<?php echo input_text('nama_ta',$nama_ta,'md-input','required');?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>aktif</label>
								<?php                    
									$op=NULL;
									$op['']='Pilih Salah Satu';                                
										$op['Y']='Y';
										$op['T']='T';
									echo select('aktif',$op,$aktif,'','required data-md-selectize');?>
							</div>
						</div>
            <div class="col-lg-12">
              <div class="form-group">
              <label></label>
                <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
              </div>
            </div>
            <div class="clearfix"></div>
            </form>
                </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

    <!--END FORM -->
<?php } else { ?>
<!--OPEN TABLE-->
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Tahun Anggaran</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Tahun Anggaran</h2>

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

        <a href="<?=site_url()?>ta?tambah" class="btn btn-success" ns-click="true" ns-title="Data Tahun Anggaran"><i class="fa fa-plus"></i> Tambah</a>
        <hr>
        <?php echo $this->session->flashdata('info');?>
        <?php echo $table;?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<?php } ?>
<!--END TABLE-->

