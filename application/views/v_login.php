<!DOCTYPE html>
<html lang="en">
  <head>
  <?php include 'html/head.php'; ?>
  <title>Halaman Masuk </title>
  <style media="screen">
    input,select,textarea{
      width: 100% !important;
      border: 1px solid #999 !important;
      box-shadow: none !important;
      border-radius: 0 !important
    }
    .btn{
      border-radius: 0 !important

    }
    .logo{
      width: 60px
    }
    body{
      background: #394 url('<?=base_url('assets/images/background.jpg')?>') 0px 0 no-repeat!important;
      /*background: #aaa !important;*/
      background-size: cover !important;
    }
    h1{
      text-shadow: none !important;
      color:#26b99a
    }
    p{
      text-shadow: none !important;
      color: #444;
      padding: 0;
      margin: 0;
      font-size: 10px
    }
    .login_content{
      background: rgba(200, 200, 200, 0.2);
      padding: 50px 30px 10px

    }
    .login_content{
      width: 100% !important;min-width:auto
    }
    .box-logo{
      width: 100px;
      overflow: hidden;
      height: 100px;
      margin: 10px auto 40px;
      padding: 13px 0 0;
      background: rgba(0, 0, 0, 0.1);
      border-radius: 50%;


    }
    .login_content h1:after,
    .login_content h1:before{
      background: #fff
    }
  </style>
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <div class="box-logo">
              <img src="<?=base_url('assets/images/logo.png')?>" alt="" class="logo">
            </div>
              <?php echo $this->session->flashdata('info');?>
              <form class="" action="" method="post">
              <h1 style="font-size:20px">HALAMAN MASUK</h1>
              <?php echo '<span style="text-align:center">'.$this->session->flashdata('info')."</span>";?>
              <div>
                <input type="text" name="username" class="form-control" placeholder="Nama Pengguna" required="" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Kata Sandi" required="" />
              </div>
              <div>
                <button type="submit" class="btn btn-success " name="login"><i class="fa fa-sign-in"></i> Masuk</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                  <h1 style="padding:0;margin:10px 0;font-size:16px"><i class="fa fa-plus"></i> KENDALI ANGGARAN</h1>
                  <p>©2017 BKPSDM KABUPATEN TANAH LAUT</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
  </body>
</html>
