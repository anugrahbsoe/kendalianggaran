<div id="container-hc">

</div>
<table id="hc-table" style="display:none">
  <thead>
    <tr>
      <th></th>
      <?php
        $this->db->where('a.id_ta',$id_ta);
        $this->db->group_by('nama_bidang');
        $ambilbidang=$this->m_pptk->get_data();
        foreach ($ambilbidang->result() as $rowbidang) {
          echo th($rowbidang->nama_bidang);
          $series[]="  {
              type: 'column',
             name: 'Tampil Data',
           }";
        }
       ?>
       <th>Total Realisasi</th>
    </tr>
  </thead>
  <tbody>
    <?php
      for ($i=1; $i <13 ; $i++) {
        if(strlen($i)==1){
          $i='0'.$i;
        }
        ?>
        <tr>
          <th><?=bulan_huruf($i)?></th>

          <?php
          $this->db->where('a.id_ta',$id_ta);
          $this->db->group_by('nama_bidang');
          $ambilbidang=$this->m_pptk->get_data();
          foreach ($ambilbidang->result() as $rowbidang) {
            $id_bidang=$rowbidang->id_bidang;
            $this->db->where('a.id_ta',$id_ta);
            $this->db->where('baa.id_bidang',$id_bidang);
            $this->db->where('extract(month from tanggal_realisasi)="'.$i.'"');
            $realisasi=$this->m_realisasi->get_realisasi();
            echo td($realisasi);
          }
           ?>
           <?php
             $id_bidang=$rowbidang->id_bidang;
             $this->db->where('a.id_ta',$id_ta);
             $this->db->where('extract(month from tanggal_realisasi)="'.$i.'"');
             $realisasi=$this->m_realisasi->get_realisasi();
             echo td($realisasi);
            ?>
        </tr>
        <?php
      }
     ?>
  </tbody>
</table>



<script src="<?php echo base_url()?>assets/Highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url()?>assets/Highcharts/js/highcharts-3d.js"></script>
<script src="<?php echo base_url()?>assets/Highcharts/js/modules/data.js"></script>
<script src="<?php echo base_url()?>assets/Highcharts/js/modules/exporting.js"></script>
<script src="<?php echo base_url()?>assets/Highcharts/js/modules/offline-exporting.js"></script>
<script src="<?php echo base_url()?>assets/Highcharts/js/themes/sand-signika.js"></script>

<script type="text/javascript">

$(function () {
   $('#container-hc').highcharts({
        series: [
          <?=implode(',',$series)?>,{
          type: 'spline',
          name: 'Total consumption',
        }]
       ,
       data: {
           table: 'hc-table'
       },
       plotOptions: {
           column: {
               stacking: 'normal',
               dataLabels: {
                   enabled: true,
               }
           }
       },
       title: {
           text: "Realisasi Anggaran <?=nama_ta()?>"
       },
       yAxis: {
           title: {
               text: 'Grafik'
           }
       },
       tooltip: {
            formatter: function () {
              console.log(this.point);
                return '<b>' + this.series.name +'<br>Rp. '+ this.point.y +'/ '+this.point.name+'</b><br/>' ;
            }
      },
      legend: {
       align: 'center',
       verticalAlign: 'bottom',
       borderColor: '#CCC',
       borderWidth: 1,
       shadow: false
     },
      credits: {
       text: '(c) TIM IT TANAH LAUT',
       position: {
           align: 'center',
           y: 0 // position of credits
       },
       style: {
           fontSize: '8px' // you can style it!
       }
   },

   });
});

</script>
