<div class="row tile_count">
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
          <?php
              $totalpengguna=$this->m_pengguna->get_data()->num_rows();
           ?>
            <span class="count_top"><i class="fa fa-users"></i> Total Pengguna</span>
            <div class="count"><?=$totalpengguna?></div>
            <span class="count_bottom">Pengguna</span>
        </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
          <?php
              $this->db->where('a.id_ta',$id_ta);
              $this->db->group_by('nama_bidang');
              $totalbidang=$this->m_pptk->get_data()->num_rows();
           ?>
            <span class="count_top"><i class="fa fa-clock-o"></i> Total Bidang</span>
            <div class="count"><?=$totalbidang?></div>
            <span class="count_bottom">Bidang</span>
        </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
          <?php
              $this->db->where('a.id_ta',$id_ta);
              $this->db->group_by('c.id_kegiatan');
              $totalkegiatan=$this->m_anggaran->get_data()->num_rows();
           ?>
            <span class="count_top"><i class="fa fa-anchor"></i> Total Kegiatan</span>
            <div class="count"><?=$totalkegiatan?></div>
            <span class="count_bottom">Kegiatan</span>
        </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
          <?php
            $this->db->where('a.id_ta',$id_ta);
            $anggaran=$this->m_anggaran->get_pagu();
           ?>
            <span class="count_top"><i class="fa fa-money"></i> Total Anggaran</span>
            <div class="count green"><?=uangindonesia($anggaran,'Ribu')?></div>
            <span class="count_bottom">Dalam Ribu Rupiah</span>
        </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
          <?php
          $this->db->where('a.id_ta',$id_ta);
          $realisasi=$this->m_realisasi->get_realisasi();
          ?>
            <span class="count_top"><i class="fa fa-check"></i> Total Realisasi</span>
            <div class="count red"><?=uangindonesia($realisasi,'Ribu')?></div>
            <span class="count_bottom">Dalam Ribu Rupiah</span>
        </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
          <?php
          $this->db->where('a.id_ta',$id_ta);
          $talangan=$this->m_realisasi->get_realisasi(true,'Talangan');
          ?>
            <span class="count_top"><i class="fa fa-tags"></i> Total Talangan</span>
            <div class="count text-orange"><?=uangindonesia($talangan,'Ribu')?></div>
            <span class="count_bottom">Dalam Ribu Rupiah</span>
        </div>
    </div>

</div>
