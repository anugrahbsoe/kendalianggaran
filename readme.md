# Aplikasi Kendali Anggaran

aplikasi sederhana untuk mencatat arus kas/realisasi anggaran yang ada di BKPSDM Kab. Tanah Laut
platform yang digunakan dalam pembuatan aplikasi antara lain :
1. PHP (CodeIgniter 3.1.4)
2. Database (MySQL)
3. Template (Gentelella | Bootstrap 3)
4. JQuery

## Support Me
For Donate : ***BRI (0239 01 023305 50 9) A.n Nasrullah Siddik*** Thanks :).
## Contact 
1. Telegram :[@as_shiddiq](http://telegram.me/@as_shiddiq)
2. Facebook : [as.shiddiq](http://fb.com/as.shiddiq)
3. Email : n.shiddiq@gmail.com

# PREVIEW

## Halaman Login
![alt text](screenshots/login.png)

## Halaman Utama
![alt text](screenshots/home.png)
